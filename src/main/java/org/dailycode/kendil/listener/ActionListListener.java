/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.listener;

import java.io.IOException;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public interface ActionListListener {

    void actionForm() throws IOException;

    void actionEdit() throws IOException;

    void actionDelete() throws IOException;

    void actionSearch() throws IOException;
}
