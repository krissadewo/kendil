/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.datamodel;

import java.lang.reflect.Field;
import java.util.Comparator;
import org.dailycode.kendil.entity.Item;
import org.primefaces.model.SortOrder;

public class LazyItemSorter implements Comparator<Item> {

    private String sortField;
    private SortOrder sortOrder;

    public LazyItemSorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    @Override
    public int compare(Item item1, Item item2) {
        try {
            Field field1 = Item.class.getDeclaredField(this.sortField);
            field1.setAccessible(true);
            Object value1 = field1.get(item1);

            Field field2 = Item.class.getDeclaredField(this.sortField);
            field2.setAccessible(true);
            Object value2 = field2.get(item2);

            int value = ((Comparable) value1).compareTo(value2);

            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException();
        }
    }
}
