/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.datamodel;

import java.lang.reflect.Field;
import java.util.Comparator;
import org.dailycode.kendil.entity.PurchaseItem;
import org.primefaces.model.SortOrder;

public class LazyPurchaseItemSorter implements Comparator<PurchaseItem> {

    private String sortField;
    private SortOrder sortOrder;

    public LazyPurchaseItemSorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    @Override
    public int compare(PurchaseItem purchaseItem1, PurchaseItem purchaseItem2) {
        try {
            Field fieldPurchase1 = PurchaseItem.class.getDeclaredField(this.sortField);
            fieldPurchase1.setAccessible(true);
            Object value1 = fieldPurchase1.get(purchaseItem1);

            Field fieldPurchase2 = PurchaseItem.class.getDeclaredField(this.sortField);
            fieldPurchase2.setAccessible(true);
            Object value2 = fieldPurchase2.get(purchaseItem2);

            int value = ((Comparable) value1).compareTo(value2);

            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException();
        }
    }
}
