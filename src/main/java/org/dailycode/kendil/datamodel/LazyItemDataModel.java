/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.dailycode.kendil.App;
import org.dailycode.kendil.dao.service.ItemService;
import org.dailycode.kendil.entity.Item;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SelectableDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class LazyItemDataModel extends LazyDataModel<Item> implements SelectableDataModel<Item>, Serializable {

    private List<Item> items = new ArrayList<>();
    private ItemService itemService;

    public LazyItemDataModel(ItemService itemService) {
        this.itemService = itemService;
    }

    @Override
    public Item getRowData(String rowKey) {
        for (Item item : items) {
            if (item.getCode().equals(rowKey)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public Object getRowKey(Item item) {
        return item.getCode();
    }

    @Override
    public void setRowIndex(final int rowIndex) {
        if (rowIndex == -1 || getPageSize() == 0) {
            super.setRowIndex(-1);
        } else {
            super.setRowIndex(rowIndex % getPageSize());
        }
    }

    @Override
    public List<Item> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, String> filters) {
        //filter
        if (!filters.isEmpty()) {
            Item item = new Item();
            item.setCode("");
            item.setName("");

            for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                try {
                    String filterProperty = it.next();
                    String filterValue = filters.get(filterProperty);

                    switch (filterProperty) {
                        case "code":
                            item.setCode(filterValue);
                            break;
                        case "name":
                            item.setName(filterValue);
                            break;
                    }
                } catch (Exception e) {
                    App.logError(this, e.getMessage());
                }
            }
            items = itemService.search(first, pageSize, item);
            this.setRowCount(itemService.countSearch(item));
        } else {
            items = itemService.getAll(first, pageSize, null, null);
            this.setRowCount(itemService.countAll());
        }

        //sort
        if (sortField != null) {
            Collections.sort(items, new LazyItemSorter(sortField, sortOrder));
        }

        //Add callback if data found or not
        boolean found = false;
        if (items.size() > 0) {
            found = true;
        }
        RequestContext.getCurrentInstance().addCallbackParam("found", found);

        return items;
    }
}
