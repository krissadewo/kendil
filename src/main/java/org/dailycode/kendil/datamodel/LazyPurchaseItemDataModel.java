/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.dailycode.kendil.App;
import org.dailycode.kendil.dao.service.PurchaseItemService;
import org.dailycode.kendil.entity.PurchaseItem;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SelectableDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class LazyPurchaseItemDataModel extends LazyDataModel<PurchaseItem> implements SelectableDataModel<PurchaseItem>, Serializable {

    private List<PurchaseItem> purchaseItems = new ArrayList<>();
    private PurchaseItemService purchaseItemService;

    public LazyPurchaseItemDataModel(PurchaseItemService bookService) {
        this.purchaseItemService = bookService;
    }

    @Override
    public PurchaseItem getRowData(String rowKey) {
        for (PurchaseItem purchaseItem : purchaseItems) {
            if (purchaseItem.getInvoiceNumber().equals(rowKey)) {
                return purchaseItem;
            }
        }
        return null;
    }

    @Override
    public Object getRowKey(PurchaseItem purchase) {
        return purchase.getInvoiceNumber();
    }

    @Override
    public void setRowIndex(final int rowIndex) {
        if (rowIndex == -1 || getPageSize() == 0) {
            super.setRowIndex(-1);
        } else {
            super.setRowIndex(rowIndex % getPageSize());
        }
    }

    @Override
    public List<PurchaseItem> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, String> filters) {
        //filter
//        if (!filters.isEmpty()) {
//            for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
//                try {
//                    String filterProperty = it.next();
//                    String filterValue = filters.get(filterProperty);
//
//                    PurchaseItem purchaseItem = new PurchaseItem();
//                    purchaseItem.setInvoiceNumber("");
//                    purchaseItem.getSupplier()
//                    purchaseItem.setPublisher("");
//
//                    switch (filterProperty) {
//                        case "title":
//                            purchaseItem.setTitle(filterValue);
//                            break;
//                        case "writer":
//                            purchaseItem.setWriter(filterValue);
//                            break;
//                        case "publisher":
//                            purchaseItem.setPublisher(filterValue);
//                            break;
//                    }
//
//                    purchaseItems = purchaseItemService.search(first, pageSize, purchaseItem);
//                    this.setRowCount(purchaseItemService.countSearch(purchaseItem));
//                } catch (Exception e) {
//                    App.logError(this, e.getMessage());
//                }
//            }
//        } else {
//            purchaseItems = purchaseItemService.getAll(first, pageSize, null, null);
//            this.setRowCount(purchaseItemService.countAll());
//        }
//
//        //sort
//        if (sortField != null) {
//            Collections.sort(purchaseItems, new LazyPurchaseSorter(sortField, sortOrder));
//        }

        return purchaseItems;
    }
}
