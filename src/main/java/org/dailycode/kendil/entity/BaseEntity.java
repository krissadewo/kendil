/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class BaseEntity implements Serializable {

    private Long id;
    private Date created = new Date();
    private Date updated = new Date();
    private Long updateBy = 666l;
    private Long createBy = 666l;
    private boolean statusDelete;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public Date getUpdated() {
        return updated;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public boolean isStatusDelete() {
        return statusDelete;
    }

    public void setStatusDelete(boolean statusDelete) {
        this.statusDelete = statusDelete;
    }

    @Override
    public String toString() {
        return "BaseEntity{" + "id=" + id + ", created=" + created + ", updated=" + updated + ", updateBy=" + updateBy + ", createBy=" + createBy + ", statusDelete=" + statusDelete + '}';
    }
}
