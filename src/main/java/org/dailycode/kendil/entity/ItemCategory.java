/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.entity;

/**
 * Jan 22, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class ItemCategory {

    private Item item;
    private Category category;

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "ItemCategory{" + "item=" + item + ", category=" + category + '}';
    }
}
