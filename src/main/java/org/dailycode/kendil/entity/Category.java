/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class Category extends BaseEntity implements Serializable {

    private String name;
    private Long parentCategory;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Long parentCategory) {
        this.parentCategory = parentCategory;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.name);
        hash = 37 * hash + Objects.hashCode(this.parentCategory);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Category other = (Category) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.parentCategory, other.parentCategory)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Category{" + "name=" + name + ", parentCategory=" + parentCategory + '}';
    }
}
