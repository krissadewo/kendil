/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.entity;

import java.io.Serializable;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class Role extends BaseEntity implements Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Role{" + "name=" + name + '}';
    }
}
