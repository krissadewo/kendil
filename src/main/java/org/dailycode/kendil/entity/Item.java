/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.entity;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class Item extends BaseEntity implements Serializable {

    private String code;
    private String name;
    private Double priceSale;
    private Double pricePurchase;
    private Integer stock;
    private List<Category> categorys;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPriceSale() {
        return priceSale;
    }

    public void setPriceSale(Double priceSale) {
        this.priceSale = priceSale;
    }

    public Double getPricePurchase() {
        return pricePurchase;
    }

    public void setPricePurchase(Double pricePurchase) {
        this.pricePurchase = pricePurchase;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public List<Category> getCategorys() {
        return categorys;
    }

    public void setCategorys(List<Category> categorys) {
        this.categorys = categorys;
    }

    @Override
    public String toString() {
        return "Item{" + "code=" + code + ", name=" + name + ", priceSale=" + priceSale + ", pricePurchase=" + pricePurchase + ", stock=" + stock + ", categorys=" + categorys + '}';
    }
}
