/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.entity;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class PurchaseItemDetail extends BaseEntity {

    private Double price;
    private Double discount;
    private Integer qty;
    private Double total;
    private Item item;
    private PurchaseItem purchaseItem;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public PurchaseItem getPurchaseItem() {
        return purchaseItem;
    }

    public void setPurchaseItem(PurchaseItem purchaseItem) {
        this.purchaseItem = purchaseItem;
    }

    @Override
    public String toString() {
        return "PurchaseItemDetail{" + "price=" + price + ", discount=" + discount + ", qty=" + qty + ", total=" + total + ", item=" + item + ", purchaseItem=" + purchaseItem + '}';
    }
}
