/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.entity;

import java.util.Date;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class SaleItem extends BaseEntity {

    private String invoicesNumber;
    private Date dateSale;

    public String getInvoicesNumber() {
        return invoicesNumber;
    }

    public void setInvoicesNumber(String invoicesNumber) {
        this.invoicesNumber = invoicesNumber;
    }

    public Date getDateSale() {
        return dateSale;
    }

    public void setDateSale(Date dateSale) {
        this.dateSale = dateSale;
    }

    @Override
    public String toString() {
        return "SaleItem{" + "invoicesNumber=" + invoicesNumber + ", dateSale=" + dateSale + '}';
    }
}
