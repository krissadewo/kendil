/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class PurchaseItem extends BaseEntity {

    private String invoiceNumber;
    private Date datePurchase;
    private Supplier supplier;
    private boolean isRevisi;
    private List<PurchaseItemDetail> purchaseItemDetails = new ArrayList<>();

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getDatePurchase() {
        return datePurchase;
    }

    public void setDatePurchase(Date datePurchase) {
        this.datePurchase = datePurchase;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public boolean isIsRevisi() {
        return isRevisi;
    }

    public void setIsRevisi(boolean isRevisi) {
        this.isRevisi = isRevisi;
    }

    public List<PurchaseItemDetail> getPurchaseItemDetails() {
        return purchaseItemDetails;
    }

    public void setPurchaseItemDetails(List<PurchaseItemDetail> purchaseItemDetails) {
        this.purchaseItemDetails = purchaseItemDetails;
    }

    @Override
    public String toString() {
        return "PurchaseItem{" + "invoiceNumber=" + invoiceNumber + ", datePurchase=" + datePurchase + ", supplier=" + supplier + ", isRevisi=" + isRevisi + ", purchaseItemDetails=" + purchaseItemDetails + '}';
    }
}
