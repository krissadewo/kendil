/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.entity;

import java.io.Serializable;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class SaleItemDetail extends BaseEntity implements Serializable{

    private Double price;
    private Double discount;
    private Integer qty;
    private Double total;
    private Item item;
    private SaleItem saleItem;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public SaleItem getSaleItem() {
        return saleItem;
    }

    public void setSaleItem(SaleItem saleItem) {
        this.saleItem = saleItem;
    }

    @Override
    public String toString() {
        return "SaleItemDetail{" + "price=" + price + ", discount=" + discount + ", qty=" + qty + ", total=" + total + ", item=" + item + ", saleItem=" + saleItem + '}';
    }
}
