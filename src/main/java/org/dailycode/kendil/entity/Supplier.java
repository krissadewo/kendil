/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.entity;

import java.io.Serializable;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class Supplier extends BaseEntity implements Serializable {

    private String name;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Supplier{" + "name=" + name + ", address=" + address + '}';
    }
}
