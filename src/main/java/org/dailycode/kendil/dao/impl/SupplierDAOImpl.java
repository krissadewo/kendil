/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.dailycode.kendil.dao.SupplierDAO;
import org.dailycode.kendil.entity.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Repository
public class SupplierDAOImpl implements SupplierDAO {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Override
    public Supplier save(Supplier entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public Supplier update(Supplier entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public Supplier delete(Supplier entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public List<Supplier> delete(List<Supplier> entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public List<Supplier> getAll() {
        String sql = "SELECT *FROM supplier";
        return jdbcTemplate.query(sql, new SupplierRowMapper());
    }
    
    @Override
    public List<Supplier> getAll(Integer start, Integer end, String sortField, String orderBy) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public List<Supplier> search(Integer start, Integer end, Supplier entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public Supplier getById(Long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public int countAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public int countSearch(Supplier entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    class SupplierRowMapper implements RowMapper<Supplier> {
        
        @Override
        public Supplier mapRow(ResultSet rs, int rowNum) throws SQLException {
            Supplier supplier = new Supplier();
            supplier.setId(rs.getLong("id"));
            supplier.setName(rs.getString("name"));
            supplier.setAddress(rs.getString("address"));
            return supplier;
        }
    }
}
