/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.dao.impl;

import java.util.List;
import org.dailycode.kendil.dao.PurchaseItemDAO;
import org.dailycode.kendil.entity.PurchaseItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Repository
public class PurchaseItemDAOImpl implements PurchaseItemDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public PurchaseItem save(PurchaseItem entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public PurchaseItem update(PurchaseItem entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public PurchaseItem delete(PurchaseItem entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<PurchaseItem> delete(List<PurchaseItem> entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<PurchaseItem> getAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<PurchaseItem> getAll(Integer start, Integer end, String sortField, String orderBy) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<PurchaseItem> search(Integer start, Integer end, PurchaseItem entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public PurchaseItem getById(Long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int countAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int countSearch(PurchaseItem entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
