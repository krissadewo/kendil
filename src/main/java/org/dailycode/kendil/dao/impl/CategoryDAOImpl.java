/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.dailycode.kendil.dao.CategoryDAO;
import org.dailycode.kendil.entity.Category;
import org.dailycode.kendil.entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Repository
public class CategoryDAOImpl implements CategoryDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Category save(Category entity) {
        String sql = "";
        return entity;
    }

    @Override
    public Category update(Category entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Category delete(Category entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Category> delete(List<Category> entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Category> getAll() {
        String sql = "SELECT *FROM category ORDER BY name ASC";
        return jdbcTemplate.query(sql, new CategoryRowMapper());
    }

    @Override
    public List<Category> getAll(Integer start, Integer end, String sortField, String orderBy) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Category> search(Integer start, Integer end, Category entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Category getById(Long id) {
        String sql = "SELECT *FROM category WHERE id = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{id}, new CategoryRowMapper());
        } catch (DataAccessException dae) {
            return null;
        }
    }

    @Override
    public Category getByParentCategory(Long id) {
        String sql = "SELECT *FROM category WHERE parent_id = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{id}, new CategoryRowMapper());
        } catch (DataAccessException dae) {
            return null;
        }
    }

    @Override
    public List<Category> getByItem(Item item) {
        String sql = "SELECT *FROM category c "
                + " INNER JOIN item_category ic "
                + " ON ic.category_id = c.id"
                + " WHERE ic.item_id = ?";

        return jdbcTemplate.query(sql, new Object[]{item.getId()}, new CategoryRowMapper());
    }

    @Override
    public int countAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int countSearch(Category entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    class CategoryRowMapper implements RowMapper<Category> {

        @Override
        public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
            Category category = new Category();
            category.setId(rs.getLong("id"));
            category.setName(rs.getString("name"));
            category.setParentCategory(rs.getLong("parent_id"));
            return category;
        }
    }
}
