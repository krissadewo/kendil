/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.dailycode.kendil.dao.ItemDAO;
import org.dailycode.kendil.entity.Category;
import org.dailycode.kendil.entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Repository
public class ItemDAOImpl implements ItemDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Item save(final Item entity) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            String sqlSaveItem = "INSERT INTO item("
                    + "code,"
                    + "name,"
                    + "price_sale,"
                    + "price_purchase,"
                    + "stock,"
                    + "create_by,"
                    + "created,"
                    + "update_by,"
                    + "updated) VALUES(?,?,?,?,?,?,?,?,?)";

            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(sqlSaveItem, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, entity.getCode());
                ps.setString(2, entity.getName());
                ps.setDouble(3, entity.getPriceSale());
                ps.setDouble(4, entity.getPricePurchase());
                ps.setInt(5, entity.getStock());
                ps.setDouble(6, entity.getCreateBy());
                ps.setTimestamp(7, new java.sql.Timestamp(entity.getCreated().getTime()));
                ps.setDouble(8, entity.getUpdateBy());
                ps.setTimestamp(9, new java.sql.Timestamp(entity.getUpdated().getTime()));
                return ps;
            }
        }, keyHolder);

        entity.setId(keyHolder.getKey().longValue());

        if (!entity.getCategorys().isEmpty()) {
            String sqlSaveItemCategory = "INSERT INTO item_category VALUES(?,?)";
            jdbcTemplate.batchUpdate(sqlSaveItemCategory, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    Category category = entity.getCategorys().get(i);
                    ps.setLong(1, entity.getId());
                    ps.setLong(2, category.getId());
                }

                @Override
                public int getBatchSize() {
                    return entity.getCategorys().size();
                }
            });
        }

        return entity;
    }

    @Override
    public Item update(final Item entity) {
        String sqlSaveItem = "UPDATE item SET "
                + "code = ?,"
                + "name = ?,"
                + "price_sale = ?,"
                + "price_purchase = ?,"
                + "stock = ?,"
                + "update_by = ?,"
                + "updated = ? "
                + "WHERE id = ?";

        jdbcTemplate.update(sqlSaveItem, new Object[]{
                    entity.getCode(),
                    entity.getName(),
                    entity.getPriceSale(),
                    entity.getPricePurchase(),
                    entity.getStock(),
                    entity.getUpdateBy(),
                    entity.getUpdated(),
                    entity.getId()});

        String sqlDeleteItemCategory = "DELETE FROM item_category WHERE item_id = ?";
        jdbcTemplate.update(sqlDeleteItemCategory, new Object[]{entity.getId()});

        if (!entity.getCategorys().isEmpty()) {
            String sqlSaveItemCategory = "INSERT INTO item_category VALUES(?,?)";
            jdbcTemplate.batchUpdate(sqlSaveItemCategory, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    Category category = entity.getCategorys().get(i);
                    ps.setLong(1, entity.getId());
                    ps.setLong(2, category.getId());
                }

                @Override
                public int getBatchSize() {
                    return entity.getCategorys().size();
                }
            });
        }

        return entity;
    }

    @Override
    public Item delete(Item entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Item> delete(List<Item> entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Item> getAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Item> getAll(Integer start, Integer end, String sortField, String orderBy) {
        String sql = " SELECT * FROM item i "
                + " WHERE i.status_delete = 'N'"
                + " ORDER BY updated DESC"
                + " LIMIT ?,?";
        return jdbcTemplate.query(sql, new Object[]{start, end}, new ItemRowMapper());
    }

    @Override
    public List<Item> search(Integer start, Integer end, Item entity) {
        String sql = "SELECT *FROM item "
                + " WHERE code LIKE ?  "
                + " AND name LIKE ?"
                + " AND status_delete = 'N'"
                + " LIMIT ?,?";

        return jdbcTemplate.query(sql, new Object[]{
                    "%" + entity.getCode() + "%",
                    "%" + entity.getName() + "%",
                    start,
                    end}, new ItemRowMapper());
    }

    @Override
    public Item getById(Long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int countAll() {
        String sql = "SELECT COUNT(id) FROM item WHERE status_delete = 'N' ";
        return jdbcTemplate.queryForInt(sql);
    }

    @Override
    public int countSearch(Item entity) {
        final String sql = " SELECT count(i.id)FROM item i "
                + " WHERE i.status_delete = 'N' "
                + " AND i.code LIKE ? "
                + " AND i.name LIKE ? ";

        return jdbcTemplate.queryForInt(sql, new Object[]{
                    "%" + entity.getCode() + "%",
                    "%" + entity.getName() + "%"});
    }

    class ItemRowMapper implements RowMapper<Item> {

        @Override
        public Item mapRow(ResultSet rs, int rowNum) throws SQLException {
            Item item = new Item();
            item.setId(rs.getLong("id"));
            item.setCode(rs.getString("code"));
            item.setName(rs.getString("name"));
            item.setPriceSale(rs.getDouble("price_sale"));
            item.setPricePurchase(rs.getDouble("price_purchase"));
            item.setStock(rs.getInt("stock"));
            return item;
        }
    }
}
