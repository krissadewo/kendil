/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.dailycode.kendil.dao;

import org.dailycode.kendil.entity.PurchaseItem;

/**
 * Jan 13, 2013
 * @author Kris Sadewo <krissadewo@dailycode.org> 
 */
public interface PurchaseItemDAO extends BaseDAO<PurchaseItem> {

}
