package org.dailycode.kendil.dao;

import java.util.List;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public interface BaseDAO<T> {

    T save(T entity);

    T update(T entity);

    T delete(T entity);

    /**
     * Batch delete
     *
     * @param entity
     * @return
     */
    List<T> delete(List<T> entity);

    List<T> getAll();

    /**
     *
     * @param start
     * @param end
     * @param sortField
     * @param orderBy
     * @return
     */
    List<T> getAll(Integer start, Integer end, String sortField, String orderBy);

    /**
     *
     * @param start
     * @param end
     * @param entity
     * @return
     */
    List<T> search(Integer start, Integer end, T entity);

    T getById(Long id);

    int countAll();

    /**
     *
     * @param entity
     * @return
     */
    int countSearch(T entity);
}
