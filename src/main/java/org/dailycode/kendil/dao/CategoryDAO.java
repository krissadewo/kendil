/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.dao;

import java.util.List;
import org.dailycode.kendil.entity.Category;
import org.dailycode.kendil.entity.Item;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public interface CategoryDAO extends BaseDAO<Category> {

    Category getByParentCategory(Long id);

    List<Category> getByItem(Item item);
}
