/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.dao.service;

import java.util.List;
import org.dailycode.kendil.dao.SupplierDAO;
import org.dailycode.kendil.entity.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Service
public class SupplierService {

    @Autowired
    private SupplierDAO supplierDAO;

    public List<Supplier> getAll() {
        return supplierDAO.getAll();
    }
}
