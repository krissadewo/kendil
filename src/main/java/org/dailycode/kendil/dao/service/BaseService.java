/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Service
public class BaseService {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private PurchaseItemService purchaseItemService;
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private ItemService itemService;

    public ItemService getItemService() {
        return itemService;
    }

    public SupplierService getSupplierService() {
        return supplierService;
    }

    public CategoryService getCategoryService() {
        return categoryService;
    }

    public PurchaseItemService getPurchaseItemService() {
        return purchaseItemService;
    }
}
