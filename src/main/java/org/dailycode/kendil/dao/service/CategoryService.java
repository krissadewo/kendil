/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.dao.service;

import java.util.List;
import org.dailycode.kendil.dao.CategoryDAO;
import org.dailycode.kendil.entity.Category;
import org.dailycode.kendil.entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Service
public class CategoryService {

    @Autowired
    public CategoryDAO categoryDAO;

    public List<Category> getAll() {
        return categoryDAO.getAll();
    }

    public List<Category> getByItem(Item item) {
        return categoryDAO.getByItem(item);
    }

    public List getAllParent() {
        List<Category> categorys = categoryDAO.getAll();
        for (Category category : categorys) {
            StringBuilder sb = new StringBuilder();
            String x = getParent(category.getId(), sb);
            System.out.println(x.substring(0, x.length() - 2));
        }
        return categorys;
    }

    public String getParent(Long parentId, StringBuilder sb) {
        Category category = categoryDAO.getById(parentId);
        if (category != null) {
            getParent(category.getParentCategory(), sb);
            sb.append(category.getName());
            sb.append(" > ");
            return sb.toString();
        } else {
            return null;
        }
    }

    public Category getById(Long id) {
        return categoryDAO.getById(id);
    }
}
