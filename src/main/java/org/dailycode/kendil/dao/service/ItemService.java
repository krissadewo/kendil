/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.dao.service;

import java.util.List;
import org.dailycode.kendil.dao.ItemDAO;
import org.dailycode.kendil.entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Service
public class ItemService {

    @Autowired
    private ItemDAO itemDAO;

    public void saveOrUpdate(Item item) {
        if (item.getId() == null) {
            itemDAO.save(item);
        } else {
            itemDAO.update(item);
        }
    }

    public List<Item> getAll() {
        return itemDAO.getAll();
    }

    public List<Item> search(int first, int pageSize, Item item) {
        return itemDAO.search(first, pageSize, item);
    }

    public List<Item> getAll(int first, int pageSize, String object, String object0) {
        return itemDAO.getAll(first, pageSize, null, null);
    }

    public int countSearch(Item item) {
        return itemDAO.countSearch(item);
    }

    public int countAll() {
        return itemDAO.countAll();
    }
}
