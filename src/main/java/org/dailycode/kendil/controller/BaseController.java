/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.controller;

import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.dailycode.kendil.App;
import org.dailycode.kendil.dao.service.BaseService;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public abstract class BaseController extends BaseService {

    @PostConstruct
    public abstract void init();
    public static final String LIST_VIEW = "list";
    public static final String FORM_VIEW = "form";

    /**
     *
     * @param severity level (warn, info ...)
     * @param message message to show
     * @param detail message detail
     */
    protected void showMessage(FacesMessage.Severity severity, String message, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, message, detail));
    }

    /**
     *
     * @param url specify native page name ex : pagex, pagey
     * @param isClearSession destroy all session data if the session scope
     * active
     */
    protected void redirectView(String url, boolean isClearSession) {
        if (isClearSession) {
            clearSessionParamValue();
        }
        redirectView(url);
    }

    protected void redirectView(String url, Object param) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("param", param);
        redirectView(url);
    }

    protected void redirectView(String url) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url.concat(".jsf"));
        } catch (IOException ex) {
            App.logError(this.getClass(), ex.getMessage());
        }
    }

    protected Object getSessionMapValue(String key) {
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(key);
    }

    protected void clearSessionParamValue() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("param");
    }

    protected Object getParamValue() {
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("param");
    }
}
