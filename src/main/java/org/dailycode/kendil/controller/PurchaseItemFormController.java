/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.dailycode.kendil.entity.Item;
import org.dailycode.kendil.entity.PurchaseItem;
import org.dailycode.kendil.entity.PurchaseItemDetail;
import org.dailycode.kendil.entity.Supplier;
import org.dailycode.kendil.listener.ActionFormListener;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Controller("purchaseItemForm")
@Scope("view")
public class PurchaseItemFormController extends BaseController implements ActionFormListener, Serializable {

    private PurchaseItem purchaseItem;
    private List<Supplier> suppliers;
    private String codeKey;
    private String nameKey;
    private Item selectedItem;
    private List<PurchaseItemDetail> purchaseItemDetails = new ArrayList<>();
    private PurchaseItemDetail selectedPurchaseItemDetail;
    private PurchaseItemDetail purchaseItemDetail = new PurchaseItemDetail();

    @Override
    public void init() {
        suppliers = getSupplierService().getAll();
        if (purchaseItem == null) {
            purchaseItem = new PurchaseItem();
        }
    }

    @Override
    public void actionCancel() throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void actionSave() throws IOException {
        codeKey = getSelectedItem().getCode();
    }

    public void addItem() {
        codeKey = getSelectedItem().toString();
        PurchaseItemDetail itemDetail = new PurchaseItemDetail();
        itemDetail.setItem(getSelectedItem());
        itemDetail.setPrice(getPurchaseItemDetail().getPrice());
        itemDetail.setQty(getPurchaseItemDetail().getQty());
        getPurchaseItemDetails().add(itemDetail);
    }

    public void removeItem() {
        purchaseItemDetails.remove(getSelectedPurchaseItemDetail());
    }

    public PurchaseItem getPurchaseItem() {
        return purchaseItem;
    }

    public void setPurchaseItem(PurchaseItem purchaseItem) {
        this.purchaseItem = purchaseItem;
    }

    public List<Supplier> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(List<Supplier> suppliers) {
        this.suppliers = suppliers;
    }

    public String getCodeKey() {
        return codeKey;
    }

    public void setCodeKey(String codeKey) {
        this.codeKey = codeKey;
    }

    public String getNameKey() {
        return nameKey;
    }

    public void setNameKey(String nameKey) {
        this.nameKey = nameKey;
    }

    public Item getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(Item selectedItem) {
        this.selectedItem = selectedItem;
    }

    public List<PurchaseItemDetail> getPurchaseItemDetails() {
        return purchaseItemDetails;
    }

    public void setPurchaseItemDetails(List<PurchaseItemDetail> purchaseItemDetails) {
        this.purchaseItemDetails = purchaseItemDetails;
    }

    public PurchaseItemDetail getSelectedPurchaseItemDetail() {
        return selectedPurchaseItemDetail;
    }

    public void setSelectedPurchaseItemDetail(PurchaseItemDetail selectedPurchaseItemDetail) {
        this.selectedPurchaseItemDetail = selectedPurchaseItemDetail;
    }

    public PurchaseItemDetail getPurchaseItemDetail() {
        return purchaseItemDetail;
    }

    public void setPurchaseItemDetail(PurchaseItemDetail purchaseItemDetail) {
        this.purchaseItemDetail = purchaseItemDetail;
    }
}
