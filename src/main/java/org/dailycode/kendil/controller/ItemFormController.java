/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import org.dailycode.kendil.entity.Category;
import org.dailycode.kendil.entity.Item;
import org.dailycode.kendil.listener.ActionFormListener;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Controller("itemForm")
@Scope("request")
public class ItemFormController extends BaseController implements ActionFormListener, Serializable {

    private Item item;
    private List<Category> categorys;

    @Override
    public void init() {
        categorys = getCategoryService().getAll();
        item = (Item) getParamValue();
        if (item == null) {
            item = new Item();
        }
    }

    @Override
    public void actionCancel() throws IOException {
        redirectView(LIST_VIEW, true);
    }

    @Override
    public void actionSave() throws IOException {
        getItemService().saveOrUpdate(item);
        redirectView(LIST_VIEW);
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public List<Category> getCategorys() {
        return categorys;
    }

    public void setCategorys(List<Category> categorys) {
        this.categorys = categorys;
    }
}
