/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import org.dailycode.kendil.datamodel.LazyPurchaseItemDataModel;
import org.dailycode.kendil.entity.PurchaseItem;
import org.dailycode.kendil.listener.ActionListListener;
import org.primefaces.model.LazyDataModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * Jan 13, 2013
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Controller("purchaseItemList")
@Scope("view")
public class PurchaseItemListController extends BaseController implements ActionListListener, Serializable {

    private LazyDataModel<PurchaseItem> dataModel;
    private List<PurchaseItem> selectedPurchaseItems;

    @Override
    public void init() {
        dataModel = new LazyPurchaseItemDataModel(getPurchaseItemService());
    }

    @Override
    public void actionForm() throws IOException {
        redirectView(FORM_VIEW);
    }

    @Override
    public void actionEdit() throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void actionDelete() throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void actionSearch() throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<PurchaseItem> getSelectedPurchaseItems() {
        return selectedPurchaseItems;
    }

    public void setSelectedPurchaseItems(List<PurchaseItem> selectedPurchaseItems) {
        this.selectedPurchaseItems = selectedPurchaseItems;
    }

    public LazyDataModel<PurchaseItem> getDataModel() {
        return dataModel;
    }

    public void setDataModel(LazyDataModel<PurchaseItem> dataModel) {
        this.dataModel = dataModel;
    }
}
