/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.dailycode.kendil.datamodel.LazyItemDataModel;
import org.dailycode.kendil.entity.Item;
import org.dailycode.kendil.listener.ActionListListener;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Controller("itemList")
@Scope("view")
public class ItemListController extends BaseController implements ActionListListener, Serializable {

    private LazyDataModel<Item> dataModel;
    private List<Item> selectedItems;
    private String searchNameKey;
    private String searchCodeKey;
    private String searchCategoryKey;

    @Override
    public void init() {
        dataModel = new LazyItemDataModel(getItemService());
    }

    @Override
    public void actionForm() throws IOException {
        redirectView("form", true);
    }

    @Override
    public void actionEdit() throws IOException {
        if (getSelectedItems().size() == 1) {
            Item item = getSelectedItems().get(0);
            item.setCategorys(getCategoryService().getByItem(item));
            redirectView(FORM_VIEW, item);
        } else if (getSelectedItems().size() > 1) {
            showMessage(FacesMessage.SEVERITY_INFO, "Data yang dipilih tidak boleh lebih dari 1", "");
        } else if (getSelectedItems().isEmpty()) {
            showMessage(FacesMessage.SEVERITY_INFO, "Pilih data terlebih dahulu", "");
        }
    }

    @Override
    public void actionDelete() throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void actionSearch() throws IOException {
        Item item = new Item();
        item.setName(searchNameKey);

        Map<String, String> filters = new HashMap<>();
        filters.put("code", searchCodeKey);
        filters.put("name", searchNameKey);

        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("itemList:dataTable");
        dataTable.setFilters(filters);

        RequestContext.getCurrentInstance().update("itemList:dataTable");
    }

    public LazyDataModel<Item> getDataModel() {
        return dataModel;
    }

    public void setDataModel(LazyDataModel<Item> dataModel) {
        this.dataModel = dataModel;
    }

    public List<Item> getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(List<Item> selectedItems) {
        this.selectedItems = selectedItems;
    }

    public String getSearchNameKey() {
        return searchNameKey;
    }

    public void setSearchNameKey(String searchNameKey) {
        this.searchNameKey = searchNameKey;
    }

    public String getSearchCodeKey() {
        return searchCodeKey;
    }

    public void setSearchCodeKey(String searchCodeKey) {
        this.searchCodeKey = searchCodeKey;
    }

    public String getSearchCategoryKey() {
        return searchCategoryKey;
    }

    public void setSearchCategoryKey(String searchCategoryKey) {
        this.searchCategoryKey = searchCategoryKey;
    }
}
