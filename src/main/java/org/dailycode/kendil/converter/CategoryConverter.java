/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.converter;

import java.util.ArrayList;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.dailycode.kendil.dao.service.BaseService;
import org.dailycode.kendil.entity.Category;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@Component
@Scope("request")
public class CategoryConverter extends BaseService implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        System.out.println("getAsObject " + getCategoryService().getById(Long.valueOf(value)));
        return getCategoryService().getById(Long.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return String.valueOf(((Category) value).getId());
    }
}
