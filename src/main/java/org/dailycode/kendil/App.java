/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil;

import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
public class App {

    private static App instance;

    private App() {
    }

    public static App getInstance() {
        if (instance == null) {
            instance = new App();
        }
        return instance;
    }

    public int countMaxYAxis(int totalMaxYAxis) {
        if (totalMaxYAxis < 6) {
            return 6;
        } else {
            return (totalMaxYAxis - (totalMaxYAxis % 6)) + 6;
        }
    }

    public static synchronized void logInfo(Object o, String message) {
        LogFactory.getLog(o.getClass()).info(message);
    }

    public static synchronized void logDebug(Object o, String message) {
        LogFactory.getLog(o.getClass()).debug(message);
    }

    public static synchronized void logError(Object o, String message) {
        LogFactory.getLog(o.getClass()).error(message);
    }
}
