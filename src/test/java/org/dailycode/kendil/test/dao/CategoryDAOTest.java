/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.kendil.test.dao;

import java.util.List;
import org.dailycode.kendil.dao.CategoryDAO;
import org.dailycode.kendil.entity.Category;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Jan 13, 2013
 *
 * @author Kris Sadewo <krissadewo@dailycode.org>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class CategoryDAOTest {

    @Autowired
    private CategoryDAO categoryDAO;

    @Before
    public void init() throws Exception {
       // System.out.println("before ");
    }

    @Test
    public void getAll() {
        List<Category> categorys = categoryDAO.getAll();
        for (Category category : categorys) {
            StringBuilder sb = new StringBuilder();
            String x = getParent(category.getId(), sb);
            System.out.println(x.substring(0, x.length() - 2));
        }
    }

    public String getParent(Long parentId, StringBuilder sb) {
        Category category = categoryDAO.getById(parentId);
        if (category != null) {
            getParent(category.getParentCategory(), sb);
            sb.append(category.getName());
            sb.append(" > ");
            return sb.toString();
        } else {
            return null;
        }
    }
}
